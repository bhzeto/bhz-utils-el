(defun search-selection (beg end)
  "search for selected text"
  (interactive "r")
  (let ((selection (buffer-substring-no-properties beg end)))
    (deactivate-mark)
    (isearch-mode t nil nil nil)
    (isearch-yank-string selection)))
(global-set-key (kbd "M-s") 'search-selection)

(defun git-grep-prompt ()
  (let* ((default (current-word))
         (prompt (if default
                     (concat "Search for: (default " default ") ")
                   "Search for: "))
         (search (read-from-minibuffer prompt nil nil nil nil default)))
    (if (> (length search) 0)
        search
      (or default ""))))

(defun git-grep (expression)
  "git-grep on the entire current repository"
  (interactive (list (git-grep-prompt)))
  (grep-find (concat "git --no-pager grep -n -i "
                     (shell-quote-argument expression)
                     " $(git rev-parse --show-toplevel)")))

(defun git-grep-dir (expression dir)
  "git-grep in specific directory within current repository"
  (interactive "sSearch expression:\nsSearch in path:")
  (grep-find (concat "git --no-pager grep -n -i "
                     (shell-quote-argument expression)
                     (concat " $(git rev-parse --show-toplevel)/" dir))))

(defun open-buffer-aside (buffer)
  "Opens buffer in window without switching the focus to it"
  (interactive "bBuffer:")
  (unless (get-buffer-window buffer 0)
    (pop-to-buffer buffer nil t)))

(defun process-buffer-filter (buf)
  "Filter for getting the output of processes from their buffer into echo area"
  (interactive "bBuffer: ")
  (switch-to-buffer buf)
  (beginning-of-buffer)
  (setq start (point))
  (search-forward-regexp "Process * finished")
  (previous-line)
  (previous-line)
  (move-end-of-line nil)
  (setq end (point))
  (kill-ring-save start end)
  (message (car kill-ring)))

(defun which (command)
  "Locates path of executable in current machine"
  (interactive "sCommand:")
  (start-process "which-process"
                 (get-buffer-create "*which*")
                 "/usr/bin/which"
                 command)
  ;; Waits for process-filter-buffer to determine whether process is finished
  ;; Does 50 retries of 0.1 seconds each
  (setq processReady 0)
  (setq retries 0)
  (setq maxRetries 50)
  (while (and
          (< retries maxRetries)
          (not (eq processReady 1)))
    (setq processReady 1)
    (condition-case thisError
        (setq output (process-buffer-filter "*which*"))
      (error (setq processReady 0)
             (setq retries (1+ retries))
             (sit-for 0.1)
             (message "Waiting for process to finish..."))))
  (kill-buffer nil)
  (message output))

(defun start-process-aside (executable arguments)
  "Runs a process and opens a side buffer for following its progress."
  (interactive "sExecutable:\nsArgs:")
  (setq bufferName (concat "*" executable "*"))
  (ignore-errors (kill-buffer bufferName))
  (start-process (concat executable "-process")
                   (get-buffer-create bufferName)
                   (which executable)
                   arguments)
    (open-buffer-aside bufferName))

(defun indent-four-right ()
  (interactive)
  (move-to-left-margin)
  (indent-to-left-margin)
  (indent-relative) (insert "    "))
;; Also setting "emergency" key for it
(global-set-key (kbd "<backtab>") 'indent-four-right)

(defun highlight-selection (beg end)
  "Highlights selected text"
  (interactive "r")
  (let (
	(selection (buffer-substring-no-properties beg end)))
    (deactivate-mark)
    (highlight-phrase selection)))
(global-set-key (kbd "C-x <end>") 'highlight-selection)
